# foss-community-feedback

This project is about garnering the feedback from the FOSS projects that depend on gitlab's CE edition. 
We want to collect from all the projects including GNOME, KDE, Freedesktop, Debian and anyone else who is
interested.

Given that we are a number of communities, this will hopefully serve as a focal point as a discussion with Gitlab management.

Please create an issue and then apply the label of the project that you are affiliated with.
